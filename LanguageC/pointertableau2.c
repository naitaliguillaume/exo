#include <stdio.h>
#include <stdlib.h>


void plus_grand_tableau(int *tab, unsigned taille)
{
    int max=0;
    for (int j=0; j<taille; ++j)
	{
		if (tab[j]>max)
		{
			max=tab[j];
		}
	}
	printf("\nLe plus grand élément de mon tableau est de %d\n",max);
}

void plus_petit_tableau(int *tab, unsigned taille)
{

    int min = tab[0];

    for (int j=1; j<taille; ++j)
	{
		if (tab[j]<min)
		{
			min=tab[j];
		}
	}
	printf("\nLe plus petit élément de mon tableau est de %d\n",min);
}


int main(void)
{
    int tab[5] = { 2, 45, 67, 89, 123 };
    plus_grand_tableau(tab,5);
	plus_petit_tableau(tab,5);
    
    return 0;
}