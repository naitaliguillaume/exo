#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.14159265359
float perim;
float taille;
float surf;
float diam;
float volcyl;
float volcone;

int main()
{
    printf("quel est la rayon du cercle\n");
    float a;
    scanf("%f",&a);
    perim = PI*(a*2);
    printf("Ce cercle a pour périmètre:\n%f\n",perim);
    surf = PI*a*a;
    printf("Ce cercle a pour surface:\n%f\n",surf);
    diam = a*2;
    printf("Ce cercle a pour diam:\n%f\n",diam);
    printf("quel est la hauteur cone ou du cylindre?\n");
    float b;
    scanf("%f",&b);
    volcyl = surf*b;
    volcone = (surf*b)/3;
    printf("le volume du cylindre est %f.\n",volcyl);
    printf("le volume du cone est %f.\n");
    return 0;
}
