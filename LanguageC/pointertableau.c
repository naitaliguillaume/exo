#include <stdio.h>
#include <stdlib.h>

void affiche_tableau(int *tab, unsigned taille)
{
    for (unsigned i = 0; i < taille; ++i)
        printf("tab[%u] = %d\n", i, tab[i]);

    int somme=0;
    for (int j=0; j<taille; ++j)
	{
		somme = tab[j]+somme;
	}
	printf("La somme de mon tableau est de %d",somme);
}


int main(void)
{
    int tab[5] = { 2, 45, 67, 89, 123 };
    affiche_tableau(tab, 5);
    
    return 0;
}