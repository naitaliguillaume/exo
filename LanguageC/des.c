#include <stdio.h>
#include <stdlib.h>
#include <time.h>   // pour rand

int de(int face,int nbdes)
{
	int somme=0;
	for(int i =0;i<nbdes;i++)
	{
	srand(time(NULL));
	int num = rand()%(face+1);
	somme=num+somme;
	}
	return(somme);
	
}

int main(void)
{
	printf("veuillez entrer le nombre de face de votre dé:\n");
	int face, res;
	scanf("%d",&face);
	printf("\nveuillez entrer le nombre de dés:\n");
	int nbdes;
	scanf("%d",&nbdes);
	res = de(face,nbdes);
	printf("Le résulta du ou de la somme des dés est de %d.\n",res);
	return 0;
}