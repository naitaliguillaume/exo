#include <stdio.h>


typedef struct vehicule
{
	char nom_marque[256];
	char nom_model[256];
	float res;
	double vit;
	int etat; // 0 en panne et 1 fonctionnel
}s_vehicule;

void cree_voiture(s_vehicule *voiture)
{
	printf("quel est la marque de votre vehicule:\n");
	scanf("%s",&voiture->nom_marque);
	printf("quel est le modele de votre vehicule:\n");
	scanf("%s",&voiture->nom_model);
	printf("quel est la vitesse max du vehicule:\n");
	scanf("%lf",&voiture->vit);
	voiture->res=10; //en littre
	voiture->etat=1; // 0 en panne et 1 fonctionnel
}

void voir_voiture(s_vehicule *voiture)
{
	printf("\n\n-------\nLes informations de ma voitures sont:\n");
	printf("  Marque: %s\n", voiture->nom_marque);
	printf("  Model: %s\n", voiture->nom_model);
	printf("  la vitesse max est: %lf km/h\n", voiture->vit);
	printf("  le niveau du réservoire est: %f\n", voiture->res);
	printf("  Etat: %d\n\n", voiture->etat);

}

int main() 
{
	s_vehicule voiture;
	cree_voiture(&voiture);
	voir_voiture(&voiture);
	return 0;
}