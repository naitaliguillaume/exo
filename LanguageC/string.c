#include <stdio.h>
#include <string.h>

int main(void)
{
	char mot1[] = "Jason";
	char mot2[] = "Bateu";
	int test = strcmp(mot1, mot2);

	if(test == 0)
		printf("Les deux mots sont les memes.\n");
	else if(test <0)
		printf("%s < %s\n", mot1, mot2);
	else
		printf("%s > %s\n", mot1, mot2);

	return 0;
}