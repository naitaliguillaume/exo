#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	float a=1.5, b=2.5;
	FILE *fichier;
	fichier=fopen("fichier.txt","r");

	if(fichier == NULL)
	{
		exit(1);
	}
	
	// Ecriture
	/*fprintf(fichier,"%f\n",a);
	fprintf(fichier,"%f\n",b);*/

	int lettre = 0;
	signed char t[256];
	while(fgets(t,255,fichier) != NULL)
	{
		printf("\n%s\n",t);
	}

	fclose(fichier) ;
	return 0;
}