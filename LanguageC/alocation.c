#include <stdio.h>
#include <stdlib.h>


int main()
{
	int nbjoueur = 0;
	int *liste_joueur = NULL;
	int i;


	printf("combien de joueur?");
	scanf("%d", &nbjoueur);
	fflush(stdin);

	liste_joueur = malloc(sizeof(int)*nbjoueur);

	if(liste_joueur == NULL)
		exit(1);

	for(i=0;i<nbjoueur;i++)
	{
		printf("joueur %d -> numero %d\n", i+1, i*3);
		liste_joueur[i]=i*3;
	}

	for(i=0;i<nbjoueur;i++)
	{
		printf("[%d]",liste_joueur[i]);
	}

	free(liste_joueur);
	return 0;
}