#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void affiche(char matrice[3][3])
{
	int i, j;
	printf(" Le tableau 2D = \n");
 
    for(i=0; i<3; i++)
    {
        for(j=0; j<3; j++)
        {
            printf("%c", matrice[i][j]);
 
        }
        printf("\n");
    }
}

void saisi(char joueur,int *ligne,int *colone)
{
	printf("\nJoueur %c où voulez vous jouer sachant qu'on démare à la ligne 0 colone 0 (0->2)? cordonée (ligne colone)\n",joueur);
	printf("Ligne:\n");
	scanf( "%d",ligne);
	printf("Colone:\n");
	scanf( "%d",colone);
	fflush(stdin);
}

void test(char joueur,char matrice[3][3])
{

	   	if( (matrice[0][0]==joueur) && (matrice[0][1]==joueur) && (matrice[0][2]==joueur) )
	   	{
			printf("\nJoueur %c vous avez gagné. Génial!!!!!!!!!\n",joueur);
			exit(EXIT_SUCCESS);
	   	}
	   	else if((matrice[1][0]==joueur) && (matrice[1][1]==joueur) && (matrice[1][2]==joueur))
	   	{
	   		printf("\nJoueur %c vous avez gagné. Génial!!!!!!!!!\n",joueur);
	   		exit(EXIT_SUCCESS);
	   	}
	   	else if((matrice[2][0]==joueur) && (matrice[2][1]==joueur) && (matrice[2][2]==joueur))
	   	{
	   		printf("\nJoueur %c vous avez gagné. Génial!!!!!!!!!\n",joueur);
	   		exit(EXIT_SUCCESS);
	   	}
	   	else if((matrice[0][0]==joueur) && (matrice[1][0]==joueur) && (matrice[2][0]==joueur))
	   	{
	   		printf("\nJoueur %c vous avez gagné. Génial!!!!!!!!!\n",joueur);
	   		exit(EXIT_SUCCESS);
	   	}
	   	else if((matrice[0][1]==joueur) && (matrice[1][1]==joueur) && (matrice[2][1]==joueur))
	   	{
	   		printf("\nJoueur %c vous avez gagné. Génial!!!!!!!!!\n",joueur);
	   		exit(EXIT_SUCCESS);
	   	}
	   	else if((matrice[0][2]==joueur) && (matrice[1][2]==joueur) && (matrice[2][2]==joueur))
	   	{
	   		printf("\nJoueur %c vous avez gagné. Génial!!!!!!!!!\n",joueur);
	   		exit(EXIT_SUCCESS);
	   	}
	   	else if((matrice[0][0]==joueur) && (matrice[1][1]==joueur) && (matrice[2][2]==joueur))
	   	{
	   		printf("\nJoueur %c vous avez gagné. Génial!!!!!!!!!\n",joueur);
	   		exit(EXIT_SUCCESS);
	   	}
	   	else if((matrice[0][2]==joueur) && (matrice[1][1]==joueur) && (matrice[2][0]==joueur))
	   	{
	   		printf("\nJoueur %c vous avez gagné. Génial!!!!!!!!!\n",joueur);
	   		exit(EXIT_SUCCESS);
	   	}
}


int main()
{
	char matrice[3][3]={{'*','*','*'}, {'*','*','*'}, {'*','*','*'}};
	affiche(matrice);
	int i,j,k;
	while(1)
	{
		//joueur O:
		k=0;
		char joueur='O';
		saisi(joueur,&i,&j);
		matrice[i][j]='O';	
		printf("\n");
		affiche(matrice);
		test(joueur,matrice);

		//joueur X:
		joueur='X';
		saisi(joueur,&i,&j);
		matrice[i][j]='X';	
		printf("\n");
		affiche(matrice);
		test(joueur,matrice);

		//test de la présence d'*:
		for(i=0; i<3; i++)
    	{
	        for(j=0; j<3; j++)
	        {
	            if(matrice[i][j]=='*')
	            {
	            	k=k+1;
	            }
	 
	        }
	   	}

	   	//quitter si la partie est terminée:
	   	if(k==0)
	   	{
	   		printf("La partie est terminé!!!!!!!\n");
	   		exit(EXIT_SUCCESS);
	   		return 0;
	   	}




	}

	return 0;


}