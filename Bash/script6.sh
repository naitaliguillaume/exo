#!/bin/bash
######################################
# script6.sh                         #
# Utilité:                           #
# Usage:                             #
# Auteur: Guillaume NAIT-ALI         #
# Mise à jour le: 02/12/2020         #
######################################


#corp du script
echo "#####################################################################################################"
echo "Entrer 1, 2, ou quitter"
echo "1 -> afficher les comptes utilisateurs"
echo "2 -> afficher les 10 dernières connections"
echo "3 -> afficher les utilisateurs connectés"
echo "q -> pour quitter le script"
echo "#####################################################################################################"
echo ""
echo ""
echo ""
echo ""
echo ""


while :
do
  read INPUT_STRING
  case $INPUT_STRING in
	1)
		echo "#####################################################################################################"
		cat /etc/passwd | gawk -F: '{ printf "%s ",$1 }'
		echo ""
		echo "#####################################################################################################"
		echo ""
		echo ""
		;;
	q)
		echo "Au revoir!"
		break
		;;
	2)
		echo "#####################################################################################################"
		last -n 10
		echo "#####################################################################################################"
		echo ""
		echo ""
		;;
	3)
		echo "#####################################################################################################"
		w
		echo "#####################################################################################################"
		echo ""
		echo ""
		;;
  esac
done